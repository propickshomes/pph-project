let express = require('express')
let app=express() 
let bodyParser=require('body-parser') 
app.use(bodyParser.json())

const cors =require('cors')
app.use(cors());



app.use('/gethouses',require('./Components/products'))
app.use('/registerUser',require('./Components/registerUser'))
app.use('/checkingUser',require('./Components/checkingUser'))
app.use('/sellingHomes',require('./Components/sellinghouses'))
app.use('/rentalHouses',require('./Components/rentalHouses'))
app.use('/getRentalHouses',require('./Components/toletHouses'))




app.listen(3005,()=>{
    console.log("server running")
})