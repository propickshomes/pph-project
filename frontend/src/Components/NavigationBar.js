import React from 'react'
import Image from '../Images/logo.png'
// import { Link } from 'react-router-dom'

function NavigationBar() {
  return (
    <div>
      <nav class="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: '#e3f2fd' }}>
        <img src={Image} alt='' height={60} width={200}></img>
        <h1 style={{ color: 'darkblue' }}>ProPicks Homes</h1>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">

          <ul class='navbar-nav mx-auto' >
            <a class="nav-link active p-2" href="/">
             <h4 style={{ color: 'crimson' }}>HomePage</h4>
            </a>
            <a class="nav-link active p-2" href="/AboutUs">
              <h4 style={{ color: 'crimson' }}>AboutUs</h4>
              </a>
            <a class="nav-link active p-2" href="/ContactUs">
              <h4 style={{ color: 'crimson' }}>ContactUs</h4>
              </a>
            <a class="nav-link active p-2" href="/Register">
              <h4 style={{ color: 'crimson' }}>Register</h4>
              </a>
            <a class="nav-link active p-2" href="/Login">
              <h4 style={{ color: 'crimson' }}>Login</h4>
              </a>
          </ul>
        </div>

      </nav>

    </div>
  )
}

export default NavigationBar