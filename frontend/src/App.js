import './App.css';
import Footer from './Components/Footer';
import NavigationBar from './Components/NavigationBar';
import HomePage from './Pages/HomePage';

function App() {
  return (
    <div>
    <NavigationBar/>
    <HomePage/>
    <Footer/>
     
    </div>
  );
}

export default App;
